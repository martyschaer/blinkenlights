#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>

#include <util.h>

#define MAX_HUE 255

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

class RainbowEffect
{
private:
    byte _rotationRate;
    float _huePerLED;
    byte _ledZero;
    byte _saturation;

public:
    RainbowEffect(byte rotationRate = 1, int hueRepeat = 1, byte saturation = 148) : _rotationRate(rotationRate), _saturation(saturation)
    {
        _huePerLED = MAX_HUE/(float)NUM_LEDS * hueRepeat;
        _ledZero = 0;
    }

    void draw()
    {
        for(int i = 0; i < NUM_LEDS; i++) {
            g_LEDs[i] = CHSV((byte)(_ledZero + _huePerLED * i) % MAX_HUE, _saturation, 255);
        }
        _ledZero += _rotationRate;
        _ledZero %= MAX_HUE;
        delay(20);
    }
};