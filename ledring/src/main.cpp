#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <CRGBW.h>
#include <properties.h>
#include <util.h>
#include <secrets.h>

#include <bounce.h>
#include <sparkle.h>
#include <lightning.h>
#include <rainbow.h>
#include <progress.h>

// FastLED with RGBW
CRGBW g_LEDs[NUM_LEDS];
CRGB *ledsRGB = (CRGB *)&g_LEDs[0];

// WiFi connection
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

void setupWiFi() {
  Serial.print("Connecting to WiFi ");
  WiFi.begin(SECRET_WIFI_SSID, SECRET_WIFI_PASS);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("\n Conected! Addr:");
  Serial.println(WiFi.localIP());
}

void setupTime() {
  timeClient.begin();
  //timeClient.setTimeOffset(UTC_OFFSET);
}

void setup()
{
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);
  FastLED.addLeds<WS2812B, LED_PIN, RGB>(ledsRGB, getRGBWsize(NUM_LEDS));
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.clear();
  FastLED.show();
  setupWiFi();
}

void loopTime() {
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
}

void loop()
{
  ProgressEffect year = ProgressEffect(&timeClient, CRGB::Yellow, 0, 29, "2021-01-01T00:00:00", "2021-12-31T23:59:59", false);
  //ProgressEffect semester = ProgressEffect(&timeClient, CRGB::Red, 30, 59, "2021-02-22T00:00:00", "2021-60-20T23:59:59", true);
  ProgressEffect semester = ProgressEffect(&timeClient, CRGB::Blue, 30, 60, "2021-02-01T00:00:00", "2021-02-21T23:59:59", true);
  //ProgressEffect month = ProgressEffect(&timeClient, CRGB(234, 60, 2), 0, 59, "2021-02-01T00:00:00", "2021-01-28T23:59:59");
  //ProgressEffect day = ProgressEffect(&timeClient, CRGB(6, 173, 162), 30, 59, "2021-01-30T00:00:00", "2021-01-30T23:59:59");

  for (;;)
  {
    loopTime();
    FastLED.clear();

    year.draw();
    semester.draw();

    FastLED.show();
  }
}