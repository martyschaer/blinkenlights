#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>

#include <util.h>
#include <vector>

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

static const CRGB bouncerColors[] = {
    CRGB::Green,
    CRGB::Red,
    CRGB::Blue,
    CRGB::Orange,
    CRGB::Indigo,
    CRGB::Cyan,
};

struct bouncer
{
    CRGB color;
    float pos;
    float delta;
    float len;
};

class BounceEffect
{
private:
    bool _fade;
    byte _fadeStrength;
    size_t _bounceCount;
    bool _precisionDraw;
    std::vector<bouncer> _bouncers;

    void simulate(bouncer *b)
    {
        b->pos += b->delta;

        // bounce on 0
        if (b->pos > NUM_LEDS - b->len || b->pos < 0)
        {
            b->delta *= -1;
        }
    }

public:
    BounceEffect(byte fade = 0,
                 size_t bounceCount = 3,
                 float speed = 1.0,
                 bool precisionDraw = false) : _fade(fade != 0),
                                               _fadeStrength(fade),
                                               _bounceCount(bounceCount),
                                               _precisionDraw(precisionDraw)
    {
        float pos = 0.0;
        for (int i = 0; i < bounceCount; i++)
        {
            _bouncers.push_back({bouncerColors[i % ARR_SIZE(bouncerColors)], // color
                                 pos + random(19),                           // starting position
                                 speed,                                      // delta
                                 4.0});                                      // length
            pos += 19;
        }
    }

    virtual void draw()
    {
        if (_fade)
        {
            fadeToBlackBy(ledsRGB, adj_NUM_LEDS, _fadeStrength);
        }
        else
        {
            FastLED.clear();
        }
        for (int i = 0; i < _bounceCount; i++)
        {
            bouncer *b = &_bouncers[i];
            simulate(b);
            if (_precisionDraw)
            {
                drawPixels(b->pos, b->len, b->color);
            }
            else
            {
                drawPixelsImprecise(b->pos, b->len, b->color);
            }
        }
        delay(20);
    }
};