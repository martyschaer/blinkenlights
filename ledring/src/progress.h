#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>

#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include <util.h>

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

class ProgressEffect
{
private:
    NTPClient *_timeClient;
    CRGB _ringColor;
    int _startLED;
    int _endLED;
    int _effectLEDs;
    time_t _start;
    time_t _end;
    unsigned long _duration;
    bool _reversed;

public:
    ProgressEffect(NTPClient *timeClient, CRGB ringColor, int startLED, int endLED, char *start, char *end, bool reversed) : _timeClient(timeClient),
                                                                                    _ringColor(ringColor),
                                                                                    _startLED(startLED),
                                                                                    _endLED(endLED),
                                                                                    _reversed(reversed)
    {
        _effectLEDs = endLED - startLED;
        _start = str2time(start);
        _end = str2time(end);
        _duration = _end - _start;
    }

    ProgressEffect(NTPClient *timeClient, CRGB ringColor, int startLED, int endLED, tm *start, tm *end, bool reversed = false) : _timeClient(timeClient),
                                                                                    _ringColor(ringColor),
                                                                                    _startLED(startLED),
                                                                                    _endLED(endLED)
    {
        _effectLEDs = endLED - startLED;
        _start = mktime(start);
        _end = mktime(end);
        _duration = _end - _start;
    }

    void draw()
    {
        unsigned long elapsed = _timeClient->getEpochTime() - _start;
        float ratio = (float)(long double)elapsed / _duration;

        float len = _effectLEDs * ratio;

        Serial.println(ratio);
        Serial.println(len);

        if(_reversed) {
            Serial.println(_endLED - len);
            drawPixels(_endLED - len, len, _ringColor);
        } else {
            Serial.println(_startLED);
            drawPixels(_startLED, len, _ringColor);
        }

        delay(1000);
    }
};