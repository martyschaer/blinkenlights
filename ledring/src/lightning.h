#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>

#include <util.h>
#include <vector>

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

class LightningEffect
{
private:
    byte _spawnRate;
    byte _state = 0;

public:
    LightningEffect(byte spawnRate = 16) : _spawnRate(spawnRate)
    {
        // NOP
    }

    virtual void draw()
    {
        FastLED.clear();
        if(_state > 0) {
            _state++;
            if(_state % 8 == 0) {
                drawPixelsImprecise(0, 15, CRGB::White);
            }

            if(_state % 48 < 2) {
                drawPixelsImprecise(20, 40, CRGB::White);
            }
        }

        if(_state >= 255) {
            _state = 0;
        }

        if(_spawnRate > random(255) && _state == 0)
        {
            _state = 1;
        }

        delay(random(20, 40));
    }
};