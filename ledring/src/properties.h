#define NUM_LEDS 60
#define adj_NUM_LEDS 80 // 60 x 4/3 for the additional byte to cope with the RGBW instead of RGB nature of the ring
#define LED_PIN 5
#define BRIGHTNESS 16

#define UTC_OFFSET 1 * 3600 // 1h in seconds
#define TIME_FMT "%Y-%m-%dT%H:%M:%S"