#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>

#include <util.h>
#include <vector>

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

class SparkleEffect
{
private:
    byte _spawnRate;
    byte _fadeRate;

public:
    SparkleEffect(byte spawnRate = 1,
                  byte fadeRate = 16) : _spawnRate(spawnRate),
                                        _fadeRate(fadeRate)
    {
        // NOP
    }

    virtual void draw()
    {
        fadeToBlackBy(ledsRGB, adj_NUM_LEDS, _fadeRate); 
        if(_spawnRate > random(255))
        {
            g_LEDs[random(NUM_LEDS)] += CHSV(random(192), 255, 255);
        }
        delay(20);
    }
};