#include <Arduino.h>
#define FASTLED_INTERNAL // supress FastLED banner
#include <FastLED.h>
#include <CRGBW.h>
#include <properties.h>

extern CRGBW g_LEDs[];
extern CRGB *ledsRGB;

#define ARR_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define randf() ((float)(random(100000)) / 100000.0)

#ifndef UTIL
#define UTIL

time_t str2time(char *input) {
  struct tm _tm;
  time_t res;
  strptime(input, TIME_FMT, &_tm);
  res = mktime(&_tm);
  return res;
}

CRGB fraction(const CRGB in, float fraction)
{
  fraction = min(1.0f, fraction); // clip at 1
  return CRGB(in).fadeToBlackBy(255 * (1.0f - fraction));
}

void drawPixelsImprecise(float fPos, float fLen, CRGB color)
{
  int iPos = (int)fPos;
  int iLen = (int)fLen;

  while (iLen > 0)
  {
    g_LEDs[iPos + iLen--] += color;
  }
  
}

void drawPixels(float fPos, float fLen, CRGB color)
{
  float remainingFirstPixel = 1.0f - (fPos - (long)(fPos));
  float amountFirstPixel = min(remainingFirstPixel, fLen); // if we want to draw less than one pixel in length
  float remaining = min(fLen, NUM_LEDS - fPos);            // how many pixels remain after the first
  int iPos = fPos;

  // draw the head pixel
  if (remaining > 0.0f)
  {
    g_LEDs[iPos % NUM_LEDS] += fraction(color, amountFirstPixel);
    iPos++;
    remaining -= amountFirstPixel;
  }

  // draw until less than a full pixel is remaining
  while (remaining > 1.0f)
  {
    g_LEDs[iPos % NUM_LEDS] += color;
    iPos++;
    remaining--;
  }

  // draw the tail pixel
  if (remaining > 0.0f)
  {
    g_LEDs[iPos % NUM_LEDS] += fraction(color, remaining);
  }
}

#endif